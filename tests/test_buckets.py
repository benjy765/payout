import sys
sys.path.append('../src')
import buckets
import unittest


class TestInitBuckSize(unittest.TestCase):
	def test_less_than_4(self):

		actual = buckets.init_buck_size(1, 1)
		expected = []
		self.assertEqual(expected, actual)

	def test_more_buckets_than_winners(self):

		actual = buckets.init_buck_size(5, 6)
		expected = []
		self.assertEqual(expected, actual)

	def test_4_buckets(self):

		actual = buckets.init_buck_size(4, 4)
		expected = [1, 1, 1, 1]
		self.assertEqual(expected, actual)

	def test_5_buckets_for_5_winners(self):

		actual = buckets.init_buck_size(5, 5)
		expected = [1, 1, 1, 1, 1]
		self.assertEqual(expected, actual)

	def test_4_buckets_for_5_winners(self):

		actual = buckets.init_buck_size(5, 4)
		expected = []
		self.assertEqual(expected, actual)

	def test_10_buckets_for_10_winners(self):

		actual = buckets.init_buck_size(10, 10)
		expected = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
		self.assertEqual(expected, actual)

	def test_5_buckets_for_10_winners(self):

		actual = buckets.init_buck_size(10, 5)
		expected = [1, 1, 1, 1, 6]
		self.assertEqual(expected, actual)

	def test_7_buckets_for_10_winners(self):

		actual = buckets.init_buck_size(10, 7)
		expected = [1, 1, 1, 1, 2, 2, 2]
		self.assertEqual(expected, actual)

class TestRemoveSize(unittest.TestCase):
	def test_empty_list(self):

		actual = buckets.remove_size([], 1)
		expected = []
		self.assertEqual(expected, actual)

	def test_remove_nothing(self):

		actual = buckets.remove_size([1, 2, 3, 4, 5], 0)
		expected = [1, 2, 3, 4, 5]
		self.assertEqual(expected, actual)

	def test_remove_1(self):

		actual = buckets.remove_size([1, 2, 3, 4, 5], 1)
		expected = [1, 2, 3, 4, 4]
		self.assertEqual(expected, actual)

	def test_remove_1_second_elem(self):

		actual = buckets.remove_size([1, 2, 3, 4, 4], 1)
		expected = [1, 2, 3, 3, 4]
		self.assertEqual(expected, actual)

	def test_remove_2(self):

		actual = buckets.remove_size([1, 2, 3, 4, 7], 2)
		expected = [1, 2, 3, 4, 5]
		self.assertEqual(expected, actual)

	def test_remove_2_first_second(self):

		actual = buckets.remove_size([1, 2, 3, 4, 5], 2)
		expected = [1, 2, 3, 3, 4]
		self.assertEqual(expected, actual)

	def test_remove_3(self):

		actual = buckets.remove_size([1, 1, 1, 1, 2, 3, 4, 5], 3)
		expected = [1, 1, 1, 1, 2, 2, 3, 4]
		self.assertEqual(expected, actual)		

class TestExtendBuckets(unittest.TestCase):
	def test_empty_list(self):

		actual = buckets.extend_buckets([1, 1, 1, 1, 2], 8)
		expected = [1, 1, 1, 1, 1, 1, 1, 2]
		self.assertEqual(expected, actual)


if __name__ == '__main__':
	unittest.main(exit=False)
