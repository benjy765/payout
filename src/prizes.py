import numpy as np 
from nice_number import get_nice_num, round_to_nice
from utils import bisection

def init_prizes(unperfect_prize, bucket_sizes):
	""" (list, list) -> list

	Return a list of nice numbers for the prize corresponding to the bucket_sizes such that the sum is less or equal to pool and the leftover.
	"""
	# Need to check if sum(bucket_sizes) == len(unperfect_prize)
	if sum(bucket_sizes) != len(unperfect_prize):
		error('Bucket sizes is incompatible with the number of prizes')
	# Take the first unperfect_prize and generate nice numbers list
	nice = get_nice_num(int(unperfect_prize[0]))
	prizes = [] # Will contains the first attempt of good prizes
	leftover = 0
	pos = 0
	for bucket in bucket_sizes:
		# rounding the first tentative prize to nearest nice number
		curr_num = (sum(unperfect_prize[pos:pos+bucket]) + leftover)/bucket
		curr_nice = round_to_nice(curr_num, nice)
		prizes.append(curr_nice)
		# Then compute leftover
		leftover =  (curr_num - curr_nice)*bucket
		pos += bucket
	return prizes, leftover


def get_unperfect_prize(num_wins, prize_pool, price1, entry_fee):
	""" (int, int, float, int) -> list

	Return a list of unperfect sizes of length num_wins with first elem = price1, sum = prize_pool with all elem >= entry_fee.
	"""
	# prize_pool > num_wins * entry_fee
	# price1 < prize_pool
	if prize_pool <= num_wins*entry_fee:
		error("Wrong choice of parameters, prize_pool must be increase.")

	def f_to_optimize(alpha):
		count = 0
		for i in range(1,num_wins+1):
			count += 1/(i**alpha)
		return prize_pool - num_wins*entry_fee - (price1-entry_fee)*count
	b = 1 - np.log((prize_pool-(num_wins*entry_fee))/(price1 - entry_fee))/np.log(num_wins)
	alpha = bisection(f_to_optimize, 0, b)
	return [entry_fee + (price1 - entry_fee)/(x**alpha) for x in range(1, num_wins+1)]

def spend_leftover(prizes, bucket_sizes, leftover):
	""" (list, list, float) -> (list, list, float)
	
	Return the finale prizes list along with the bucket_sizes and leftover when all the leftover is spend.
	"""
	nice_numbers = get_nice_num(prizes[0])
	# First : Spend as much of possible leftover on singleton bucket 2 through 4.
	for i in range(1, 4):
		min_val = min(prizes[i] + leftover, (prizes[i-1] + prizes[i])/2)
		nice_val = round_to_nice(min_val, nice_numbers)
		leftover += prizes[i] - nice_val
		prizes[i] = nice_val
		if leftover == 0: # Could choose another value
			return prizes, bucket_sizes, leftover
		# Otherwise, we try to adjust starting form the final bucket.
		bucket_num = len(bucket_sizes)-1
	while bucket_num > 0:
		while leftover >= bucket_sizes[bucket_num]:
			prizes[bucket_num] += 1 # Could lead to nice number violations
			leftover -= bucket_sizes[bucket_num]
			if leftover == 0: # Could choose another value
				return prizes, bucket_sizes, leftover
		bucket_num -= 1
		if leftover%prizes[bucket_num] == 0:
			bucket_sizes[bucket_num] += leftover/prizes[bucket_num] # number of winners increase
	return prizes, bucket_sizes, leftover