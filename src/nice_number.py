def get_nice_num(max_num):
	""" (num) -> list

	Return a list of sorted list of nice numbers 
	"""
	nice_numbers = []
	for i in range(1, int(max_num)+1):
		if is_nice_num(i):
			nice_numbers.append(i)
	return nice_numbers

def is_nice_num(num):
	""" (num) -> bool

	Return True if num is a nice number, otherwise return False
	"""
	while num > 1000:
		num = num/10
	# Conditions : 
	if num >= 250:
		return num%50 == 0
	elif num >= 100:
		return num%25 == 0
	elif num >= 10:
		return num%5 == 0
	elif num > 0:
		return isinstance(num, int)
	else:
		return False

def round_to_nice(num_to_round, nice_numbers):
	""" (float, list of int) -> int

    Return the nearest nice number less than num_to_round.

    >>> round_to_nice(6, [1, 2, 5, 10, 15, 20])
    5
    >>> round_to_nice(1, [1, 2])
    1
    >>> round_to_nice(1001, [1, 2, 100, 1000, 10000])
    1000
    """
	if len(nice_numbers) == 0:
		return []
	if num_to_round >= nice_numbers[-1]:
		return nice_numbers[-1]
	if num_to_round < nice_numbers[0]:
		return []
	min_idx = 0
	max_idx = len(nice_numbers) - 1
	idx = (max_idx + min_idx)//2
	curr_val = nice_numbers[idx]
	next_val = nice_numbers[idx+1]
	while curr_val > num_to_round or num_to_round >= next_val:
		# Increase the index. 
		if curr_val < num_to_round:
			min_idx = idx
		elif curr_val > num_to_round:
			max_idx = idx
		idx = (max_idx + min_idx)//2
		curr_val = nice_numbers[idx]
		next_val = nice_numbers[idx+1]
	return curr_val
