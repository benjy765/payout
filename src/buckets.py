from utils import bisection
from math import ceil, floor

def init_buck_size(num_wins, num_bucks):
	""" (int, int) -> list of int

	Return an ordered list of bucket size of length num_bucks such that the sum equals num_wins.
	"""
	if num_wins < 4:
		# Must be at least 4 winners
		return [] 
	if num_wins < num_bucks:
		# Must be more or an equal number of winners than the number of buckets.
		return []
	if num_wins > 4 and num_bucks <= 4:
		# The first four buckets have size 1.
		return []
	bucket_sizes = [1 for i in range(0, 4)] # First 4 buckets of size 1
	if num_wins - sum(bucket_sizes) == 1:
		bucket_sizes.append(1) # Size of bucket 5 = 1
		return bucket_sizes 
	if num_wins == num_bucks:
		bucket_sizes += [1 for i in range(0, num_wins-4)]
		return bucket_sizes
	def b_to_optimize(beta):
		# This is for the first 4 buckets size = 1
		count = 0
		for i in range(1, num_bucks-3):
			count += beta**i
		return count - num_wins + 4
	beta = bisection(b_to_optimize, -1, 2)
	sum_buck = 5
	i = 1
	while sum_buck <= num_wins:
		this_buck_size = ceil(beta**i)
		bucket_sizes.append(this_buck_size)
		sum_buck += this_buck_size
		i += 1
	to_remove = sum(bucket_sizes) - num_wins
	if to_remove > 0:
		# We need to decrease some sizes
		bucket_sizes = remove_size(bucket_sizes, to_remove)
	if len(bucket_sizes) < num_bucks:
		bucket_sizes = extend_buckets(bucket_sizes, num_bucks)
	to_remove = sum(bucket_sizes) - num_wins
	if to_remove > 0:
		# We need to decrease some sizes
		bucket_sizes = remove_size(bucket_sizes, to_remove)	
	return bucket_sizes

def remove_size(bucket_sizes, to_remove):
	""" (list of int, int) -> list of int
	
	Return a updated bucket_sizes list with the amount to_remove removed.
	This function is called when sum(bucket_sizes) > num_wins
	"""
	# There's two things that can happen: length(bucket_sizes) < num_buck
	if len(bucket_sizes) == 0:
		return []
	idx = len(bucket_sizes) - 1
	while to_remove > 0:
		# Backward and Forward pass
		diff = bucket_sizes[idx] - bucket_sizes[idx-1]
		if diff >= to_remove:
			bucket_sizes[idx] -= to_remove
			to_remove = 0
		else:
			bucket_sizes[idx] -= diff
			to_remove -= diff
			idx -= 1
	if to_remove != 0:
		remove_size(bucket_sizes, to_remove)
	return bucket_sizes

def extend_buckets(bucket_sizes, num_bucks):
	""" (list of int, int) -> list of int
	
	Return a updated bucket_sizes list with the correct length, 
	that is len(bucket_sizes) = num_bucks. This function is called
	when len(bucket_sizes) < num_bucks
	"""
	num_buck_to_add = num_bucks - len(bucket_sizes) 
	# First try to extend with ones. 
	ones_to_add = [1 for i in range(0, num_buck_to_add)]
	return bucket_sizes[0:4]+ ones_to_add + bucket_sizes[4:]

"""
Note: Some errors can occurs, like init_buck_size(15, 12)

Typically errors will occurs when num_wins is near num_bucks.
"""