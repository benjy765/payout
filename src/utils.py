import csv
from numpy import dot

def bisection(fun, a, b):
    # Ensure that we have the right interval
    while fun(a)*fun(b) > 0:
        b += 1
    c = (a+b)/2.0
    while abs(fun(c)) > 0.01:
        if fun(a)*fun(c) < 0:
            b = c
        else:
            a = c
        c = (a+b)/2.0
    return c

def step_axis(bucket_size, init_prizes):
    x_axis = [0]
    y_axis = [init_prizes[0]] + init_prizes
    scale = 0
    for size in bucket_size:
        scale += size
        x_axis.append(scale)
    return x_axis, y_axis

def write_to_csv(prizes, bucket_size, leftover, file_name='payout.csv'):
    csvData = [['Position', 'Prize']]
    curr_buck = 0
    for i in range(0, len(prizes)):
        curr_buck += bucket_size[i]
        if bucket_size[i] == 1:
            csvData.append([str(curr_buck), str(prizes[i])])
        else:
            low_num = curr_buck - bucket_size[i] + 1
            pos_range = str(low_num)+'-'+str(curr_buck)
            csvData.append([pos_range, str(prizes[i])])
    with open(file_name, 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData)
        writer.writerow([])
        writer.writerow(['Leftover', str(leftover)])
        writer.writerow(['Total Prize', str(compute_total_prize(prizes, bucket_size))])
        writer.writerow(['Total Winners', str(sum(bucket_size))])
    csvFile.close()

def compute_total_prize(prizes, bucket_size):
    return dot(prizes, bucket_size)

