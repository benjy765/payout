# Payout structure for e-sport

Python 3 implementation of a payout structure for e-sport. 
For a complete description of this module, I refer to the 
*Payout_tutorial* notebook in the Tutorial directory.

This implementation is mainly based on the following publication:
* [Determining Tournament Payout Structures for Daily Fantasy Sports](https://arxiv.org/pdf/1601.04203.pdf)
